package ud4_ex3;

public class ex3 {

	public static void main(String[] args) {
		
		/*3. Escribe un programa Java que realice lo siguiente: declarar dos variables X e Y de tipo int,
		dos variables N y M de tipo double y asigna a cada una un valor. A continuación muestra por
		pantalla:*/

		int x = 30;
		int y = 20;
		
		double n = 10.30;
		double m = 5.20;
		
		int sumaXY = x+y;
		int diferenciaXY = x-y;
		int productoXY = x*y;
		int cocienteXY = x/y;
		int restoXY = x%y;
		
		double sumaNM = n+m;
		double diferenciaNM = n-m;
		double productoNM = n*m;
		double cocienteNM = n/m;
		double restoNM = n%m;
		
		double sumaXN = x+n;
		double cocienteYM = y/m;
		double restoYM = y%m;
		
		
		double sumaTotal = sumaXY+diferenciaXY+productoXY+cocienteXY+restoXY+sumaNM+diferenciaNM+productoNM+cocienteNM+restoNM+sumaXN+cocienteYM+restoYM;
		double productoTotal = sumaXY*diferenciaXY*productoXY*cocienteXY*restoXY*sumaNM*diferenciaNM*productoNM*cocienteNM*restoNM+sumaXN*cocienteYM*restoYM;
		
		System.out.println("El valor de cada variable:");
		System.out.println("La suma X + Y: " + sumaXY);
		System.out.println("La diferencia X – Y: " + diferenciaXY);
		System.out.println("El producto X * Y: " + productoXY);
		System.out.println("El cociente X / Y : " + cocienteXY);
		System.out.println("El resto X % Y : " + restoXY);
		System.out.println("La suma N + M: " + sumaNM);
		System.out.println("La diferencia N – M: " + diferenciaNM);
		System.out.println("El producto N * M: " + productoNM);
		System.out.println("El cociente N / M: " + productoNM);
		System.out.println("El resto N % M: " + restoNM);
		System.out.println("La suma X + N: " + sumaXN);
		System.out.println("El cociente Y / M: " + cocienteYM);
		System.out.println("El resto Y % M: " + restoYM);
		System.out.println("La suma de todas las variables: " + sumaTotal);
		System.out.println("El producto de todas las variables: " + productoTotal);
	}

}
